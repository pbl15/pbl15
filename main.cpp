#include <windows.h>
#include <time.h> 
#include <iostream>
#include <math.h>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#define FRAME_HEIGHT 480
#define FRAME_WIDTH 640
#define FRAME_RATE 60
#define IF_LASER_MOVE_CONSTANT 7
#define DISTANCE_TO_CHANGE_SLIDE 150
#define MINIMAL_DISTANCE_TO_CHANGE_SLIDE 30
#define DISTANCE_IN_Y_FOR_CHANGE_SLIDE 50
#define DISTANCE_IN_X_FOR_CHANGE_SLIDE 50
#define DISTANCE_FROM_SIDES_FOR_ARROW_EVENT 50

#define MIN_TRESHOLD 240 //for grayscale

#define DO_NOTHING_MODE -1
#define PRESENTER_MODE -2
#define MOUSE_MODE -3
#define PRESENTER_MODE_WITH_MOUSE -4

using namespace cv;
using namespace std;

vector<Point2f> perspectivePoints;
int screenWidth = GetSystemMetrics(SM_CXSCREEN);
int screenHeight = GetSystemMetrics(SM_CYSCREEN);

Point lastLaserPosition = Point(-1, -1); //for drawingLine function
int helper_drawLine = 0;

Point initialPointPositionForLeftClick; // for double mouse click
int helper_initialPointPositionForLeftClick = 0, count_initialPointPositionForLeftClick = 0; //for double mouse click

Point initialPointPositionForLeftOrRightArrow = Point(-1,-1); //for left and right arrow event
int helper_initialPointPositionForLeftOrRightArrow = 0; 

//grayscale conversion
Mat imgGray, imgGrayBlurred;
double minVal, maxVal;
Point minLoc, maxLoc;
//end of grayscale conversion

//basic functions
void callBackFunc(int event, int x, int y, int flags, void* userdata); //get point in a window by clicking
Mat afterMorphOpening(Mat& input); //perform morphological opening
Mat afterPerspective(Mat &input, Point2f inputPoints[], Point2f outputPoints[]); // perform perspective transform
void dotPosition(Mat &input, Point &laserPosition, int &dArea); // calculate dot position
void drawLine(Point &laserDot, Mat &imgLines); // draw line 
Point2d recalculateMouseCoordinate(Point2d laserDot); // recalculate place of laser in addition to screen
void rgbTresholdValues(int &iLowH, int &iHighH, int &iLowS, int &iHighS, int &iLowV, int &iHighV);
void setCursorPosition(Point2d laserDot); //sets cursor position
void controlTrackbarWindow(int &iLowH, int &iHighH, int &iLowS, int &iHighS, int &iLowV, int &iHighV);
void getPerspectivePoints(Mat input);
void setPerspectivePoints(Point2f inputPoints[], Point2f outputPoints[]);
bool ifLaserMoving(Point A, Point B);
int distanceInX(Point A, Point B); //calculate distance in along X axis between two points
int distanceInY(Point A, Point B); //calculate distance in along X axis between two points
//End of basic functions

//rgb conversion
bool ifLaserDetectedRGB(Mat input); // detection of laser RGB
//end of rgb conversion

//gray scale conversion
void grayScaleConversion(Mat imgPerspective); //conversion to grayscale and set maxLoc, minLoc, maxVal, minVal
bool ifLaserDetectedGRAY(double val); // detection of laser GRAY
//end of gray scale conversion

//Mouse mode
void doubleLeftMouseClick(Point input);
void leftMouseClick(Point input);
void leftMouseDoubeClickEvent(Point laserDot, Point lastLaserPosition);
//end of mouse mode

//Presenter mode
void arrowEvent(Point &input, Mat inputMat); //arrows event in presentation mode
//end of presenter mode

int lastActiveMode = -1;

int main(int argc, char** argv)
{
	//camera detection
	VideoCapture test(1);
	int detectedCamera = 1;
	if (!test.isOpened())
	{
		detectedCamera = 0;
	}
	VideoCapture cap(detectedCamera);
	if (!cap.isOpened())
	{
		cout << "Cannot open the web cam" << endl;
		return -1;
	}
	//end of camera detection

	//set camera 
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	cap.set(CV_CAP_PROP_FPS, FRAME_RATE);
	//end of set camera

	int iLowH, iHighH, iLowS, iHighS, iLowV, iHighV;
	rgbTresholdValues(iLowH, iHighH, iLowS, iHighS, iLowV, iHighV);

	controlTrackbarWindow(iLowH, iHighH, iLowS, iHighS, iLowV, iHighV);

	Mat imgTmp;
	cap.read(imgTmp);
	Mat imgLines = Mat::zeros(imgTmp.size(), CV_8UC3);

	while (true) // process frames from camera
	{
		Mat imgOriginal, output;
		bool bSuccess = cap.read(imgOriginal);
		if (!bSuccess) // in case of failure
		{
			cout << "Cannot read a frame from video stream" << endl;
			waitKey(3000);
			break;
		}
		
		Point2f inputPoints[4], outputPoints[4];
		if (perspectivePoints.size() >= 4)
		{
			setPerspectivePoints(inputPoints, outputPoints); // how does it work ??!?!
			output = afterPerspective(imgOriginal, inputPoints, outputPoints);
			Mat imgThresholded;

			inRange(output, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded);

			Mat imgTreshAfterOpening = afterMorphOpening(imgThresholded);

			int dArea;
			Point laserDot, mousePoint;
			dotPosition(imgTreshAfterOpening, laserDot, dArea);

			if (ifLaserDetectedRGB(imgTreshAfterOpening))
			{
				if (lastActiveMode == -1)// do nothing
				{
					
				}
				else if (lastActiveMode == -2)// presenter mode
				{
					arrowEvent(laserDot, imgTreshAfterOpening); 
				}
				else if (lastActiveMode == -3 )// mouse mode
				{
					setCursorPosition(laserDot);
					leftMouseDoubeClickEvent(laserDot, lastLaserPosition);
				}
				else if (lastActiveMode == -4) //presenter mode with drawing on slide (mouse move)
				{
					setCursorPosition(laserDot);
					//mouse_event(MOUSEEVENTF_LEFTDOWN, laserDot.x, laserDot.y, 0, 0);
					if (helper_initialPointPositionForLeftOrRightArrow == 0)
						initialPointPositionForLeftOrRightArrow = laserDot;
					else if (laserDot.y - initialPointPositionForLeftOrRightArrow.y >= DISTANCE_TO_CHANGE_SLIDE && distanceInX(initialPointPositionForLeftOrRightArrow, laserDot) < DISTANCE_IN_X_FOR_CHANGE_SLIDE)
					{
						lastActiveMode = -2;
						//mouse_event(MOUSEEVENTF_LEFTUP, laserDot.x, laserDot.y, 0, 0);
						cout << "Return to presentation mode" << endl;
						initialPointPositionForLeftOrRightArrow = Point(-99, -99);
						helper_initialPointPositionForLeftOrRightArrow = -1;
					}
					else if (helper_initialPointPositionForLeftOrRightArrow == 25)
					{
						helper_initialPointPositionForLeftOrRightArrow = -1;
					}
					helper_initialPointPositionForLeftOrRightArrow++;
				}
				drawLine(laserDot, imgLines);
			}
			else //if laser not detected
			{
				helper_drawLine = 0; //helper for drawing line

				helper_initialPointPositionForLeftOrRightArrow = 0;

				if (count_initialPointPositionForLeftClick == 1) // helper for doubleLeft click function
				{
					count_initialPointPositionForLeftClick = 2;
				}
			}
			output = output + imgLines;
			imshow("Thresholded Image", imgThresholded);
			imshow("Original", imgOriginal); 
			imshow("After perspective", output);
			imshow("After morph opening", imgTreshAfterOpening);
		}
		else
		{
			cvDestroyWindow("Thresholded Image");
			cvDestroyWindow("After perspective");
			getPerspectivePoints(imgOriginal);
		}

		if (waitKey(1) == 8) // key with backspace on it
		{
			imgLines = Mat::zeros(imgLines.size(), CV_8UC3);
		}
		else if (waitKey(1) == 27) //esc button
		{
			cout << "Esc key is pressed by user" << endl;
			break;
		}
		else if (waitKey(1) == 32) //is spacebar
		{
			imgLines = Mat::zeros(imgLines.size(), CV_8UC3);
			perspectivePoints.clear();
			system("cls");
		}
		else if (waitKey(1) == 49) // key with 1 on it
		{
			lastActiveMode = -1; //do nothing mode
			cout << "You are in do nothing mode" << endl;
		}
		else if (waitKey(1) == 50) // key with 2 on it
		{
			lastActiveMode = -2;// presenter mode
			cout << "You are in presenter mode" << endl;
		}
		else if (waitKey(1) == 51) // key with 3 on it
		{
			lastActiveMode = -3; // mouse mode
			cout << "You are in mouse mode" << endl;
		}
	}
	return 0;
}












void callBackFunc(int event, int x, int y, int flags, void* userdata)
{
	if (flags == (EVENT_FLAG_LBUTTON))
	{
		cout << "Left mouse button is clicked - position (" << x << ", " << y << ")" << endl;
		Point vertice;
		vertice.x = x;
		vertice.y = y;
		perspectivePoints.push_back(vertice);
	}
}
Mat afterMorphOpening(Mat& input)
{
	Mat output, afterErode;
	erode(input, afterErode, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
	dilate(afterErode, output, getStructuringElement(MORPH_ELLIPSE, Size(8, 8)));
	return output;
};

Mat afterPerspective(Mat &input, Point2f inputPoints[], Point2f outputPoints[])
{
	Mat lambda(2, 4, CV_32FC1), output;
	lambda = Mat::zeros(FRAME_HEIGHT, FRAME_WIDTH, input.type());
	lambda = getPerspectiveTransform(inputPoints, outputPoints);
	warpPerspective(input, output, lambda, output.size());
	return output;
};

void dotPosition(Mat &input, Point &laserPosition, int &dArea)
{
	Moments oMoments = moments(input);
	laserPosition.x = (int)(oMoments.m10 / oMoments.m00);
	laserPosition.y = (int)(oMoments.m01 / oMoments.m00);
	dArea = (int)oMoments.m00;
}

bool ifLaserDetectedRGB(Mat input)
{
	Moments oMoments = moments(input);
	if ((int)oMoments.m00 != 0)
		return true;
	return false;
};
bool ifLaserDetectedGRAY(double val)
{
	if ((int)val > MIN_TRESHOLD)
		return true;
	return false;
};

void drawLine(Point &laserDot, Mat &imgLines)
{
	if (helper_drawLine == 0)
	{
		lastLaserPosition = laserDot;
		helper_drawLine = 1;
	}
	else if (laserDot.x >= 0 && laserDot.y >= 0 && lastLaserPosition.x >= 0 && lastLaserPosition.y >= 0 && helper_drawLine == 2)
	{
		line(imgLines, lastLaserPosition, laserDot, Scalar(0, 255, 0), 2);
		lastLaserPosition = laserDot;
	}
	else if (helper_drawLine == 1)
	{
		helper_drawLine++;
	}
};

Point2d recalculateMouseCoordinate(Point2d laserDot)
{
	double distanceOfOnePixelX = screenWidth / (float)FRAME_WIDTH;
	double distanceOfOnePixelY = screenHeight / (float)FRAME_HEIGHT;

	Point2d pointOfMouse;
	pointOfMouse.x = distanceOfOnePixelX * laserDot.x;
	pointOfMouse.y = distanceOfOnePixelY * laserDot.y;
	return pointOfMouse;
};

void rgbTresholdValues(int &iLowH, int &iHighH, int &iLowS, int &iHighS, int &iLowV, int &iHighV)
{
	iLowH = 187;
	iHighH = 255;

	iLowS = 255;
	iHighS = 255;

	iLowV = 180;
	iHighV = 255;
};

void setCursorPosition(Point2d laserDot)
{
	Point mousePoint;
	mousePoint = recalculateMouseCoordinate(laserDot);
	SetCursorPos(mousePoint.x, mousePoint.y);
};

void controlTrackbarWindow(int &iLowH, int &iHighH, int &iLowS, int &iHighS, int &iLowV, int &iHighV)
{
	namedWindow("Control", CV_WINDOW_AUTOSIZE);
	createTrackbar("LowH", "Control", &iLowH, 255); //Hue (0 - 179) rgb to 255
	createTrackbar("HighH", "Control", &iHighH, 255);

	createTrackbar("LowS", "Control", &iLowS, 255);
	createTrackbar("HighS", "Control", &iHighS, 255);

	createTrackbar("LowV", "Control", &iLowV, 255);
	createTrackbar("HighV", "Control", &iHighV, 255);
};

void getPerspectivePoints(Mat input)
{
	imshow("Original", input);
	setMouseCallback("Original", callBackFunc, NULL);
};

void setPerspectivePoints(Point2f inputPoints[], Point2f outputPoints[])
{
	inputPoints[0] = perspectivePoints[0];
	inputPoints[1] = perspectivePoints[1];
	inputPoints[2] = perspectivePoints[2];
	inputPoints[3] = perspectivePoints[3];

	outputPoints[0] = Point2f(0, 0);
	outputPoints[1] = Point2f(FRAME_WIDTH, 0);
	outputPoints[2] = Point2f(FRAME_WIDTH, FRAME_HEIGHT);
	outputPoints[3] = Point2f(0, FRAME_HEIGHT);
}

bool ifLaserMoving(Point A, Point B)
{
	//cout << A << " " << B << endl; //to check if works well
	if (abs(A.x - B.x) >= IF_LASER_MOVE_CONSTANT || abs(A.y - B.y) >= IF_LASER_MOVE_CONSTANT)
		return true;
	return false;
};

void doubleLeftMouseClick(Point input)
{
	mouse_event(MOUSEEVENTF_LEFTDOWN, input.x, input.y, 0, 0);
	mouse_event(MOUSEEVENTF_LEFTUP, input.x, input.y, 0, 0);
	mouse_event(MOUSEEVENTF_LEFTDOWN, input.x, input.y, 0, 0);
	mouse_event(MOUSEEVENTF_LEFTUP, input.x, input.y, 0, 0);
};

void leftMouseClick(Point input)
{
	mouse_event(MOUSEEVENTF_LEFTDOWN, input.x, input.y, 0, 0);
	mouse_event(MOUSEEVENTF_LEFTUP, input.x, input.y, 0, 0);
};

void leftMouseDoubeClickEvent(Point laserDot, Point lastLaserPosition)
{
	helper_initialPointPositionForLeftClick++;
	if (!ifLaserMoving(laserDot, lastLaserPosition) && count_initialPointPositionForLeftClick == 0)
	{
		count_initialPointPositionForLeftClick = 1;
		initialPointPositionForLeftClick = laserDot;
		//cout << "laser stays in one position" << endl;
	}
	else if (!ifLaserMoving(laserDot, lastLaserPosition) && helper_initialPointPositionForLeftClick == 5)
	{
		leftMouseClick(recalculateMouseCoordinate(initialPointPositionForLeftClick));
		//cout << "single click" << endl;
	}
	else if (!ifLaserMoving(initialPointPositionForLeftClick, lastLaserPosition) && count_initialPointPositionForLeftClick == 2 && distanceInX(initialPointPositionForLeftClick,laserDot) <= 5 && distanceInY(initialPointPositionForLeftClick, laserDot) <= 5)
	{
		doubleLeftMouseClick(recalculateMouseCoordinate(initialPointPositionForLeftClick));
		helper_initialPointPositionForLeftClick = 0;
		count_initialPointPositionForLeftClick = 0;
		//cout << "double click performeddddddddddddddddddddddddddddddd" << endl;
	}
	else if (helper_initialPointPositionForLeftClick >= 10)
	{
		helper_initialPointPositionForLeftClick = 0;
		count_initialPointPositionForLeftClick = 0;
	}
	//cout << "helper " << helper_initialPointPositionForLeftClick << " count " << count_initialPointPositionForLeftClick << endl;
}

void arrowEvent(Point &input, Mat img)
{
	if (ifLaserDetectedRGB(img))
	{
		if (helper_initialPointPositionForLeftOrRightArrow == 0)
			initialPointPositionForLeftOrRightArrow = input;
		else if ((initialPointPositionForLeftOrRightArrow.x - input.x >= DISTANCE_TO_CHANGE_SLIDE || (input.x < DISTANCE_FROM_SIDES_FOR_ARROW_EVENT && initialPointPositionForLeftOrRightArrow.x - input.x >= MINIMAL_DISTANCE_TO_CHANGE_SLIDE)) && distanceInY(initialPointPositionForLeftOrRightArrow, input) < DISTANCE_IN_Y_FOR_CHANGE_SLIDE)
		{
			//left key
			keybd_event(VK_RIGHT, 0xCD, 0, 0);
			keybd_event(VK_RIGHT, 0xCD, KEYEVENTF_KEYUP, 0);
			cout << "left arrow pressed" << endl;
			initialPointPositionForLeftOrRightArrow = Point(-99,-99);
			helper_initialPointPositionForLeftOrRightArrow = 20;
		}
		else if ((input.x - initialPointPositionForLeftOrRightArrow.x >= DISTANCE_TO_CHANGE_SLIDE || (input.x > FRAME_WIDTH - DISTANCE_FROM_SIDES_FOR_ARROW_EVENT && input.x - initialPointPositionForLeftOrRightArrow.x >= MINIMAL_DISTANCE_TO_CHANGE_SLIDE)) && distanceInY(initialPointPositionForLeftOrRightArrow, input) < DISTANCE_IN_Y_FOR_CHANGE_SLIDE)
		{
			//right key
			keybd_event(VK_LEFT, 0xCB, 0, 0);
			keybd_event(VK_LEFT, 0xCB, KEYEVENTF_KEYUP, 0);
			cout << "right arrow pressed" << endl;
			initialPointPositionForLeftOrRightArrow = Point(-99, -99);
			helper_initialPointPositionForLeftOrRightArrow = 20;
		}
		else if (initialPointPositionForLeftOrRightArrow.y - input.y >= DISTANCE_TO_CHANGE_SLIDE && distanceInX(initialPointPositionForLeftOrRightArrow, input) < DISTANCE_IN_X_FOR_CHANGE_SLIDE)
		{
			// event going from bottom to the top
			// q pressed (in prezi q stands for laser pointer enalbe/disable
			keybd_event(0x51, 0x90, 0, 0);
			keybd_event(0x51, 0x90, KEYEVENTF_KEYUP, 0);
			lastActiveMode = -4; //set mode to presenter with drawing mode (mouse move)
			cout << "drawing mode" << endl;
			initialPointPositionForLeftOrRightArrow = Point(-99, -99);
			helper_initialPointPositionForLeftOrRightArrow = 20;
		}
		else if (input.y - initialPointPositionForLeftOrRightArrow.y >= DISTANCE_TO_CHANGE_SLIDE && distanceInX(initialPointPositionForLeftOrRightArrow, input) < DISTANCE_IN_X_FOR_CHANGE_SLIDE)
		{
			// event going from top to the bottom
			// b pressed (in prezi b stands for black screen
			keybd_event(0x42, 0xB0, 0, 0);//b button press
			keybd_event(0x42, 0xB0, KEYEVENTF_KEYUP, 0);
			cout << "blank screen" << endl;
			initialPointPositionForLeftOrRightArrow = Point(-99, -99);
			helper_initialPointPositionForLeftOrRightArrow = 20;
		}
		else if (helper_initialPointPositionForLeftOrRightArrow == 10)
			helper_initialPointPositionForLeftOrRightArrow = -1;
		else if (helper_initialPointPositionForLeftOrRightArrow == 25)
		{
			helper_initialPointPositionForLeftOrRightArrow = -1;
		}
		helper_initialPointPositionForLeftOrRightArrow++;
	}
	else
	{
		helper_initialPointPositionForLeftOrRightArrow = 0;
		initialPointPositionForLeftOrRightArrow = Point();
	}
}

int distanceInX(Point A, Point B)
{
	return abs(A.x - B.x);
}

int distanceInY(Point A, Point B)
{
	return abs(A.y - B.y);
}

void grayScaleConversion(Mat imgPerspective)
{
	cvtColor(imgPerspective, imgGray, CV_BGR2GRAY);
	GaussianBlur(imgGray, imgGrayBlurred, Size(3, 3), 0);
	minMaxLoc(imgGrayBlurred, &minVal, &maxVal, &minLoc, &maxLoc);
}